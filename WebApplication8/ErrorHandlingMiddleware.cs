﻿public class ErrorHandlingMiddleware
{
    readonly RequestDelegate next;
    readonly WebApplication app;

    public ErrorHandlingMiddleware(RequestDelegate next, WebApplication app)
    {
        this.next = next;
        this.app = app;
    }
    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await next.Invoke(context);
        }
        catch (Exception ex)
        {
            app.Logger.LogError($"Error: {ex.Message} Time:{DateTime.Now.ToLongTimeString()}");
            context.Response.Redirect("/");
        }
    }
}
