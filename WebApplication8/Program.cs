var builder = WebApplication.CreateBuilder(args);
builder.Logging.AddProvider(new FileLoggerProvider(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt")));
var app = builder.Build();

app.UseMiddleware<ErrorHandlingMiddleware>(app);

app.MapGet("/", async (context) =>
{
    var valueFromCookies = context.Request.Cookies["WrittenData"];
    var response = context.Response;
    response.ContentType = "text/html; charset=utf-8";

    await response.WriteAsync(@"
                <form method=""post"" action=""/form"">
                    <label for=""cookie"">Cookie value:</label>
                    <input type=""text"" name=""cookie"" /><br />
                    <label for=""date"">Date and time:</label>
                    <input type=""datetime-local"" name=""date"" /><br />
                    <button type=""submit"" >Submit</button>
                </form>
            ");

    await response.WriteAsync($"Stored value: {valueFromCookies}");
});

app.MapPost("/form", (HttpContext context) =>
{

    var cookie = context.Request.Form["cookie"];
    var date = context.Request.Form["date"];

    DateTime inputDate = DateTime.Parse(date);

    CookieOptions options = new CookieOptions();

    options.Expires = inputDate;
    context.Response.Cookies.Append("WrittenData", cookie, options);

    context.Response.Redirect("/");
});


app.Run();