﻿public static class FileLoggerExtensions
{
    public static ILoggingBuilder AddFile(ILoggingBuilder builder, string filePath)
    {
        builder.AddProvider(new FileLoggerProvider(filePath));
        return builder;
    }
}